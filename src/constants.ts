export const API = 'https://www.reddit.com/r/programming/top.json'
export const CHECK_NETWORK_INTERVAL = 1000
export const REQUEST_LIMIT = 20
export const CACHE_INVALIDATE_TIMEOUT = 1000 * 60
export const SYNC_INTERVAL = 1000 * 60
