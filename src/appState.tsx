import { AppState as RNAppState } from "react-native"
import { useMount } from '~/hooks'

const AppState = () => {

    const handleAppStateChange = (nextAppState: string) => {
        if (nextAppState === 'active') {

        }
        if (nextAppState === 'background') {

        }
    }

    useMount(() => {
        RNAppState.addEventListener("change", handleAppStateChange);
        return () => {
            RNAppState.removeEventListener("change", handleAppStateChange);
        }
    })

    return null
}

export default AppState