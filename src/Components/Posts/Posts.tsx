import equal from 'fast-deep-equal/react'
import React, { useCallback } from 'react'
import { FlatList, SafeAreaView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getPosts, syncPosts } from '~/Actions/ActionApp'
import Post from '~/Components/Post/Post'
import { State } from '~/Reducers'
import styles from './PostsStyles'

export interface PostsState {
    posts: Post[],
    refreshing: boolean,
}

const stateSelector = (state: State): PostsState => {
    return {
        posts: state.ReducerApp.posts,
        refreshing: state.ReducerApp.refreshing,
    }
}

const Posts = () => {

    const dispatch = useDispatch()
    const state = useSelector<State, PostsState>(stateSelector, equal)
    const {
        posts,
        refreshing
    } = state

    const renderItem = useCallback(({ item, index }: { item: Post, index: number }) => {
        return (
            <Post
                key={`Post-${index}`}
                {...item}
            />
        )
    }, [])

    const onEndReached = () => {
        dispatch(getPosts())
    }

    const onRefresh = () => {
        dispatch(syncPosts(false, true))
    }

    return (
        <SafeAreaView style={styles.wrapper}>
            <FlatList
                refreshing={refreshing}
                onRefresh={onRefresh}
                style={styles.list}
                renderItem={renderItem}
                data={posts}
                keyExtractor={(item: Post, index: number) => `post-${item.id}`}
                onEndReached={onEndReached}
                onEndReachedThreshold={0.5}
            />
        </SafeAreaView>
    )
}

export default Posts