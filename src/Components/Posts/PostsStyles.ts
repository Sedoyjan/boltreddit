import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    list: {
        flex: 1,
        paddingHorizontal: 16
    },
});

export default styles