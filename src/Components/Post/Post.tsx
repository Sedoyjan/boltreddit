import equal from 'fast-deep-equal/react'
import React, { memo, useCallback } from 'react'
import { Linking, Text, TouchableOpacity, View } from 'react-native'
import styles from './PostStyles'

export interface Post_Props extends Post {

}

const Post = (props: Post_Props) => {

    const {
        id,
        title,
        subreddit_name_prefixed,
        url,
        score,
        name
    } = props

    const onPress = useCallback(() => {
        Linking.openURL(url)
    }, [url])

    return (
        <TouchableOpacity
            style={styles.wrapper}
            onPress={onPress}
        >
            <Text style={styles.subreddit}>{subreddit_name_prefixed}</Text>
            <View style={styles.row}>
                <View style={styles.content}>
                    <Text style={styles.title}>{title}</Text>
                </View>
                <View style={styles.score}>
                    <Text>{score}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default memo(Post, equal)