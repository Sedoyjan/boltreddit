import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        minHeight: 44,
        paddingTop: 8,
        paddingBottom: 8,
        zIndex: 1
    },
    subreddit: {
        color: '#666',
        marginBottom: 4,
    },
    row: {
        flexDirection: 'row'
    },
    content: {
        flexDirection: 'column',
        flex: 1
    },
    title: {
        fontSize: 16
    },
    score: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        minWidth: 50,
        marginLeft: 8,
        fontSize: 16
    },
});

export default styles