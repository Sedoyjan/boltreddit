import React from 'react';
import { StatusBar } from 'react-native'
import { useDispatch } from 'react-redux';
import { appInit } from '~/Actions/ActionApp';
import Header from '~/Components/Header/Header';
import Posts from '~/Components/Posts/Posts';
import ErrorMessage from '~/Components/ErrorMessage/ErrorMessage';
import { useMount } from '~/hooks';


const App = () => {

    const dispatch = useDispatch()

    useMount(() => {
        dispatch(appInit())
    })

    return (
        <>
            <StatusBar
                barStyle={'light-content'}
                backgroundColor={'#2eb875'}
            />
            <Header />
            <Posts />
        </>
    )
}

export default App