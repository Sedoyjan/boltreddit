import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#2eb875',
        position: 'relative',
        zIndex: 2
    },
    wrapper: {
        height: 44,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        backgroundColor: '#2eb875',
        zIndex: 4
    },
    title: {
        fontWeight: 'bold',
        fontSize: 28,
        color: '#fff'
    },
    icons: {
        flexDirection: 'row',
    },
    noSignalIcon: {
        marginLeft: 4
    },
    error: {
        position: 'absolute',
        height: 44,
        bottom: 0,
        backgroundColor: '#FF7947',
        left: 0,
        width: '100%',
        zIndex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        justifyContent: 'space-between'
    },
    errorText: {
        fontWeight: 'bold'
    },
    button: {
        backgroundColor: '#fff',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontWeight: 'bold'
    },
});

export default styles