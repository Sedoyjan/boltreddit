import { useDispatch, useSelector } from 'react-redux'
import equal from 'fast-deep-equal/react'
import React, { memo, useEffect, useRef } from 'react'
import { View, Text, SafeAreaView, ActivityIndicator, Image, Animated, TouchableOpacity } from 'react-native'
import styles from './HeaderStyles'
import { State } from '~/Reducers'
import { setHasError, getPosts, syncPosts } from '~/Actions/ActionApp'

export interface HeaderState {
    fetching: boolean,
    refreshing: boolean,
    isConnected: boolean,
    hasError: boolean,
    posts: Post[]
}

const stateSelector = (state: State): HeaderState => {
    return {
        fetching: state.ReducerApp.fetching,
        refreshing: state.ReducerApp.refreshing,
        isConnected: state.ReducerApp.isConnected,
        hasError: state.ReducerApp.hasError,
        posts: state.ReducerApp.posts,
    }
}

const NoSignalIcon = () => {
    return (
        <Image
            width={20}
            height={20}
            style={styles.noSignalIcon}
            source={require('./images/no-signal.png')}
        />
    )
}

const Header = () => {

    const dispatch = useDispatch()
    const translateY = useRef(new Animated.Value(0)).current
    const state = useSelector<State, HeaderState>(stateSelector, equal)
    const {
        fetching,
        refreshing,
        isConnected,
        hasError,
        posts
    } = state

    useEffect(() => {
        Animated.timing(
            translateY,
            {
                toValue: hasError ? 44 : 0,
                duration: 200,
                useNativeDriver: true
            }
        ).start();
    }, [hasError])

    const isActivityIndicatorVisible = fetching && !refreshing

    const onPress = () => {
        dispatch(setHasError(false))
        if (posts.length > 0) {
            dispatch(syncPosts())
        } else {
            dispatch(getPosts())
        }
    }

    return (
        <SafeAreaView style={styles.background}>
            <View style={styles.wrapper}>
                <Text style={styles.title}>{'Bolt Reddit'}</Text>
                <View style={styles.icons}>
                    {isActivityIndicatorVisible ? (
                        <ActivityIndicator
                            size={'small'}
                            color={'#fff'}
                        />
                    ) : null}
                    {isConnected ? null : (
                        <NoSignalIcon />
                    )}
                </View>
            </View>
            <Animated.View
                style={{
                    ...styles.error,
                    transform: [{
                        translateY: translateY,
                    }]
                }}
            >
                <Text style={styles.errorText}>{'Network error'}</Text>
                <TouchableOpacity style={styles.button} onPress={onPress}>
                    <Text style={styles.buttonText}>{'Try again'}</Text>
                </TouchableOpacity>
            </Animated.View>
        </SafeAreaView>
    )
}

export default memo(Header, equal)