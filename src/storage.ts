import AsyncStorage from '@react-native-async-storage/async-storage';

class Storage {
    selector: string = 'boltredditposts'
    clearState = (): Promise<any> => {
        return AsyncStorage.clear()
    }
    getState = (): Promise<PersistedState> => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(this.selector).then((value: string | null) => {
                if (value) {
                    resolve(JSON.parse(value) as PersistedState)
                } else {
                    reject()
                }
            })
        })
    }
    setState = (state: PersistedState) => {
        return AsyncStorage.setItem(this.selector, JSON.stringify(state))
    }
}

const storage = new Storage()

export default storage