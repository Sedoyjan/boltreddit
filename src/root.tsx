import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import { Actions } from '~/Actions';
import { } from '~/Actions/ActionApp';
import AppState from '~/appState';
import App from '~/Components/App/App';
import persistMiddleware from '~/middlewares/persistMiddleware';
import reducer, { State } from '~/Reducers';

const middleware = [thunk as ThunkMiddleware<State, Actions>, persistMiddleware]

export const store = createStore(
    reducer,
    applyMiddleware(...middleware)
)

const Root = () => {
    return (
        <Provider store={store}>
            <AppState />
            <App />
        </Provider>
    )
}

export default Root