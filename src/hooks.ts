import { useEffect } from 'react'

export type Func = () => void

export const useMount = (func: Func) => useEffect(() => func(), []);