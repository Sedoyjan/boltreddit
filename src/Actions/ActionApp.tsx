import NetInfo from "@react-native-community/netinfo";
import SplashScreen from 'react-native-splash-screen';
import { AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import {
  SetHasError,
  SetIsConnected,
  SetIsFetching,
  SetIsRefreshing,
  SetPosts
} from '~/Actions/ActionAppTypes';
import { fetchPosts, fetchPostsChunks } from '~/api';
import { CACHE_INVALIDATE_TIMEOUT, SYNC_INTERVAL } from '~/constants';
import { State } from '~/Reducers';
import storage from '~/storage';

export const APP_INIT = 'APP_INIT'
export const appInit = (): ThunkAction<void, State, any, AnyAction> => (dispatch: ThunkDispatch<State, any, AnyAction>, getState: () => State) => {

  NetInfo.addEventListener(state => {
    dispatch(setIsConnected(state.isConnected))
  })

  const fetchNewPosts = () => {
    SplashScreen.hide()
    dispatch(getPosts(true))
    dispatch(syncPosts(true))
  }

  storage.getState().then(persistedState => {
    const { datetime, posts, after } = persistedState
    const cacheTimeDelta = Date.now() - datetime
    dispatch(setPosts(posts, true, after))
    if (cacheTimeDelta > CACHE_INVALIDATE_TIMEOUT) {
      storage.clearState().then(() => {
        fetchNewPosts()
      })
    } else {
      SplashScreen.hide()
      dispatch(syncPosts(true, false, true))
    }
  }).catch(() => {
    fetchNewPosts()
  })
}

export const syncPosts = (repeat: boolean = false, isRefresh: boolean = false, immediate: boolean = false): ThunkAction<void, State, any, AnyAction> => (dispatch: ThunkDispatch<State, any, AnyAction>, getState: () => State) => {
  const sync = () => {
    const state = getState()
    if (isRefresh) {
      dispatch(setIsRefreshing(true))
    }
    const totalCount = state.ReducerApp.posts.length
    dispatch(setIsFetching(true))
    fetchPostsChunks(totalCount).then(r => {
      dispatch(setPosts(r.posts, true, r.after))
      dispatch(setHasError(false))
    }).catch((err: Error) => {
      dispatch(setHasError(true))
    }).finally(() => {
      dispatch(setIsFetching(false))
      dispatch(setIsRefreshing(false))
      if (repeat) {
        dispatch(syncPosts(repeat))
      }
    })
  }

  if (immediate) {
    sync()
  }
  if (repeat) {
    setTimeout(sync, SYNC_INTERVAL);
  } else {
    sync()
  }
}

export const GET_POSTS = 'GET_POSTS'
export const getPosts = (forceUpdate: boolean = false): ThunkAction<void, State, any, AnyAction> => (dispatch: ThunkDispatch<State, any, AnyAction>, getState: () => State) => {
  const state = getState()
  console.log(state.ReducerApp.allPostsLoaded, forceUpdate)
  if (state.ReducerApp.allPostsLoaded && !forceUpdate) {
    return
  }
  dispatch(setIsFetching(true))
  fetchPosts(state.ReducerApp.after).then(r => {
    const {
      posts,
      after
    } = r
    dispatch(setPosts(posts, forceUpdate, after))
  }).catch((err: Error) => {
    dispatch(setHasError(true))
  }).finally(() => {
    dispatch(setIsFetching(false))
  })
}

export const SET_POSTS = 'SET_POSTS'
export const setPosts = (posts: Post[], forceUpdate: boolean = false, after: string | null): SetPosts => {
  return {
    type: SET_POSTS,
    payload: {
      posts,
      after,
      forceUpdate
    }
  }
}

export const SET_IS_REFRESHING = 'SET_IS_REFRESHING'
export const setIsRefreshing = (refreshing: boolean): SetIsRefreshing => {
  return {
    type: SET_IS_REFRESHING,
    payload: {
      refreshing
    }
  }
}

export const SET_IS_FETCHING = 'SET_IS_FETCHING'
export const setIsFetching = (fetching: boolean): SetIsFetching => {
  return {
    type: SET_IS_FETCHING,
    payload: {
      fetching
    }
  }
}

export const SET_IS_CONNECTED = 'SET_IS_CONNECTED'
export const setIsConnected = (isConnected: boolean): SetIsConnected => {
  return {
    type: SET_IS_CONNECTED,
    payload: {
      isConnected
    }
  }
}

export const SET_HAS_ERROR = 'SET_HAS_ERROR'
export const setHasError = (hasError: boolean): SetHasError => {
  return {
    type: SET_HAS_ERROR,
    payload: {
      hasError
    }
  }
}