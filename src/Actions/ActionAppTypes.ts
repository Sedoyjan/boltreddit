export type SET_POSTS = 'SET_POSTS'
export interface SetPosts {
    type: SET_POSTS,
    payload: {
        posts: Post[],
        forceUpdate: boolean,
        after: string | null
    }
}

export type SET_IS_REFRESHING = 'SET_IS_REFRESHING'
export interface SetIsRefreshing {
    type: SET_IS_REFRESHING,
    payload: {
        refreshing: boolean
    }
}

export type SET_IS_FETCHING = 'SET_IS_FETCHING'
export interface SetIsFetching {
    type: SET_IS_FETCHING,
    payload: {
        fetching: boolean
    }
}

export type SET_IS_CONNECTED = 'SET_IS_CONNECTED'
export interface SetIsConnected {
    type: SET_IS_CONNECTED,
    payload: {
        isConnected: boolean
    }
}

export type SET_HAS_ERROR = 'SET_HAS_ERROR'
export interface SetHasError {
    type: SET_HAS_ERROR,
    payload: {
        hasError: boolean
    }
}

export type ActionApp = SetPosts |
    SetIsRefreshing |
    SetIsFetching |
    SetIsConnected |
    SetHasError