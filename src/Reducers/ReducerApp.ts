import { Actions } from '../Actions';

import {
    SET_POSTS,
    SET_IS_REFRESHING,
    SET_IS_FETCHING,
    SET_IS_CONNECTED,
    SET_HAS_ERROR
} from '~/Actions/ActionApp';

export interface State {
    init: boolean,
    after: string | null,
    posts: Post[],
    allPostsLoaded: boolean,
    refreshing: boolean,
    fetching: boolean,
    isConnected: boolean,
    hasError: boolean,
}

const initialState: State = {
    refreshing: false,
    init: false,
    after: null,
    posts: [],
    allPostsLoaded: false,
    fetching: false,
    isConnected: false,
    hasError: false,
}

export const ReducerApp = (state: State = initialState, action: Actions): State => {
    switch (action.type) {
        case SET_HAS_ERROR: {
            return {
                ...state,
                hasError: action.payload.hasError
            }
        }
        case SET_IS_CONNECTED: {
            return {
                ...state,
                isConnected: action.payload.isConnected
            }
        }
        case SET_IS_FETCHING: {
            return {
                ...state,
                fetching: action.payload.fetching
            }
        }
        case SET_IS_REFRESHING: {
            return {
                ...state,
                refreshing: action.payload.refreshing
            }
        }
        case SET_POSTS: {
            const posts = action.payload.forceUpdate ? action.payload.posts : [...state.posts, ...action.payload.posts]
            return {
                ...state,
                posts: posts,
                after: action.payload.after,
                allPostsLoaded: action.payload.after === null,
                hasError: false
            }
        }

        default:
            return state
    }
}
