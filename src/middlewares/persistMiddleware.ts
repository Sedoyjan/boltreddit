import { InteractionManager } from 'react-native';
import { AnyAction, Store } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { Actions } from '~/Actions';
import {
    SET_POSTS
} from '~/Actions/ActionApp';
import { State } from '~/Reducers';
import Storage from '~/storage';


const allowedActionTypes: Array<string> = [
    SET_POSTS
]

const persistMiddleware = (store: Store<State, Actions>) => (next: ThunkDispatch<State, any, AnyAction>) => (action: Actions): State => {
    let result = next(action)
    if (allowedActionTypes.includes(action.type)) {
        const interactionPromise = InteractionManager.runAfterInteractions(() => {
            let nextState = store.getState()
            const persistedState: PersistedState = {
                datetime: Date.now(),
                posts: nextState.ReducerApp.posts,
                after: nextState.ReducerApp.after
            }
            Storage.setState(persistedState).then(() => {
                interactionPromise.cancel()
            }).catch(() => {
                interactionPromise.cancel()
            })
        })
    }
    return result
}

export default persistMiddleware