import axios from 'axios'
import { API, REQUEST_LIMIT } from '~/constants'

export const fetchPostsChunks = (count: number): Promise<FetchPostsResponse> => {
    return new Promise<FetchPostsResponse>((resolve, reject) => {
        let updatedPosts: Post[] = []
        let lastAfter: string | null = null

        const fetchChunk = () => {
            fetchPosts(lastAfter).then(r => {
                updatedPosts = [...updatedPosts, ...r.posts]
                lastAfter = r.after
                if (updatedPosts.length >= count || !r.after) {
                    resolve({
                        posts: updatedPosts,
                        after: lastAfter
                    })
                } else {
                    fetchChunk()
                }
            }).catch((err: Error) => {
                reject(err)
            })
        }
        fetchChunk()
    })
}

export const fetchPosts = (afterId: string | null): Promise<FetchPostsResponse> => {
    return new Promise<FetchPostsResponse>((resolve, reject) => {
        let url = `${API}?limit=${REQUEST_LIMIT}`
        if (afterId) {
            url += `&after=${afterId}`
        }
        axios.get<RedditAPIResponse>(url).then(r => {
            if (r.data && r.data.data) {
                const {
                    after,
                    children
                } = r.data.data
                const posts: Post[] = children.map(item => {
                    return {
                        id: item.data.id,
                        title: item.data.title,
                        subreddit_name_prefixed: item.data.subreddit_name_prefixed,
                        url: item.data.url,
                        score: item.data.score,
                        name: item.data.name,
                    }
                })
                resolve({
                    posts,
                    after
                })
            } else {
                reject()
            }
        }).catch((err: Error) => {
            reject(err)
        })
    })
}