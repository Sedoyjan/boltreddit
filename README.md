## Bolt Reddit ##


![type-coverage](https://img.shields.io/badge/type--coverage-100%25-blue)

![Logo](readmeFiles/logo.png)
### Description ###

This app shows top posts of ```/r/programming``` on Reddit.
It is written with React Native using Typescript and Redux.
### Features ###


✅ - Typescript code-coverage ```1233/1233 100.00%```

✅ - Posts feed has infinite scrolling

✅ - Messages are loaded in batches of 20

✅ - Messages are saved to ```AsyncStorage```

✅ - App has sync logic, which fetch posts update every ```SYNC_INTERVAL``` (1 min)

✅ - App has ```CACHE_INVALIDATE_TIMEOUT```(2 min) for saved messages, and if timeDelta is bigger than ```CACHE_INVALIDATE_TIMEOUT``` all messages in ```AsyncStorage``` removes 

✅ - ```Posts``` list has pull-to-refresh feature

✅ - There is an ```ActivityIndicator``` in ```Header``` which shows if data is fetching

![fetching](readmeFiles/fetching.png)

✅ - Error message if something gone wrong

![errorMessage](readmeFiles/errorMessage.png)

✅ - Special icon which shows network connection problems

![noInternet](readmeFiles/noInternet.png)

### ScreenShots ###
#### iOS ####
![iPhone](readmeFiles/iPhone.png)
#### Android ####
![Android](readmeFiles/Android.png)


### Instalation ###

``` bash
# install npm packages
yarn -i

#install iOS pods
yarn pods

#start packager
yarn start
```

